#!/usr/bin/env python3
"""
Copyright (C) 2021 Cosimo Agati

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License
as published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU AGPLv3 with this software,
if not, please visit <https://www.gnu.org/licenses/>
"""

import csv
import networkx as nx
from networkx.algorithms.components.connected import connected_components

INPUTS_FILENAME = 'inputs.csv'
OUTPUTS_FILENAME = 'outputs.csv'
TRANSACTIONS_FILENAME = 'transactions.csv'


def read_csv_file(filename):
    """Open a file named filename and return a generator containing, for each
    row of the file, a list containing the values in the respective columns.
    For simplicity, they are converted to integers.

    """
    with open(filename) as csv_file:
        for row in csv.reader(csv_file, delimiter=','):
            yield [int(x) for x in row]


def get_inputs(filename):
    """Build the main transaction input dictionary from filename.  The main
    input dictionary is indexed by input id and each entry is itself a
    dictionary whose keys are 'id', 'tx_id', 'sig_id' and 'output_id'.

    """
    input_dict = {}
    for row in read_csv_file(filename):
        input_dict[row[0]] = {
            'id': row[0],
            'tx_id': row[1],
            'sig_id': row[2],
            'output_id': row[3]
        }
    return input_dict


def get_outputs(filename):
    """Build the main transaction output dictionary from filename.  The main
    output dictionary is indexed by output id and each entry is itself a
    dictionary whose keys are 'id', 'tx_id', 'pk_id' and 'value'.

    """
    output_dict = {}
    for row in read_csv_file(filename):
        output_dict[row[0]] = {
            'id': row[0],
            'tx_id': row[1],
            'pk_id': row[2],
            'value': row[3]
        }
    return output_dict


def get_transactions(filename):
    """Build the main transaction dictionary from filename.  The main
    transaction dictionary is indexed by transaction id and each entry is
    itself a dictionary whose keys are 'id', and 'block_id'.

    """
    tx_dict = {}
    for row in read_csv_file(filename):
        tx_dict[row[0]] = {'id': row[0], 'block_id': row[1]}
    return tx_dict


def is_invalid_input_id(inputs, input_id):
    """Return True if transaction input having id 'id' is badly formed, False
    otherwise.

    """
    return (inputs[input_id]['output_id'] <= 0
            or inputs[input_id]['sig_id'] <= 0)


def filter_malformed_inputs(inputs):
    """Destructively remove malformed input entries from the inputs dictionary.

    """
    invalid_input_ids = [
        input_id for input_id in inputs
        if is_invalid_input_id(inputs, input_id)
    ]
    for input_id in invalid_input_ids:
        inputs.pop(input_id)
    return inputs


def remove_incorrect_inputs(inputs, outputs):
    """Destructively remove transaction inputs that do not correctly match
    outputs.  A valid transaction input should consume an existing transaction
    output (represented in its output_id field) and its signature should match
    the output's public key.  An transaction input which does not satisfy these
    conditions is invalid and must be discarded.

    """
    bad_input_ids = []

    for input_id in inputs:
        output_id = inputs[input_id]['output_id']
        if output_id not in outputs:
            bad_input_ids.append(input_id)

    for input_id in bad_input_ids:
        inputs.pop(input_id)


def get_consumed_output_ids(inputs):
    """Return a list containing all the output ids which are consumed by an
    existing transaction input.

    """
    return [inputs[id]['output_id'] for id in inputs]


def get_utxo_ids(inputs, outputs):
    """Return a list containing the output ids corresponding to UTXOs.  These
    are, by definition, transaction outputs which have not yet been consumed by
    any transaction input.

    """
    used_transaction_ids = get_consumed_output_ids(inputs)
    return set(outputs.keys()) - set(used_transaction_ids)


def get_cumulative_utxo(inputs, outputs):
    """Return the total value of UTXOs.

    """
    utxo_ids = get_utxo_ids(inputs, outputs)
    total = 0

    for utxo_id in utxo_ids:
        total += outputs[utxo_id]['value']
    return total


def find_highest_utxo(inputs, outputs, transactions):
    """Find the UTXO with the highest value.  Return a dictionary containing
    additional information about this UTXO, namely: the transaction ID, the
    block ID, the output ID, the corresponding address ( public key) and the
    actual value.

    """
    utxo_ids = get_utxo_ids(inputs, outputs)
    max_value = 0
    max_value_id = 0

    for utxo_id in utxo_ids:
        current_value = outputs[utxo_id]['value']
        if current_value > max_value:
            max_value = current_value
            max_value_id = utxo_id

    max_utxo_transaction = transactions[outputs[max_value_id]['tx_id']]
    return {
        'tx_id': max_utxo_transaction['id'],
        'block_id': max_utxo_transaction['block_id'],
        'output_index': max_value_id,
        'address': outputs[max_value_id]['pk_id'],
        'value': max_value
    }


def get_transaction_input_addresses(inputs):
    """Return a dictionary that maps transaction IDs to the list of
    corresponding inputs.

    """
    transaction_inputs_dict = {}

    for input_id in inputs:
        tx_id = inputs[input_id]['tx_id']
        address = inputs[input_id]['sig_id']
        if tx_id not in transaction_inputs_dict:
            transaction_inputs_dict[tx_id] = []
        transaction_inputs_dict[tx_id].append(address)
    return transaction_inputs_dict


# Terrible code duplication...
def get_transaction_output_addresses(outputs):
    """Return a dictionary that maps transaction IDs to the list of
    corresponding outputs.

    """
    transaction_outputs_dict = {}

    for output_id in outputs:
        tx_id = outputs[output_id]['tx_id']
        address = outputs[output_id]['pk_id']
        if tx_id not in transaction_outputs_dict:
            transaction_outputs_dict[tx_id] = []
        transaction_outputs_dict[tx_id].append(address)
    return transaction_outputs_dict


def to_edges(component):
    """ treat `component` as a Graph and returns its edges
    to_edges(['a','b','c','d']) -> [(a,b), (b,c),(c,d)]
    """
    iterator = iter(component)
    last = next(iterator, None)

    if last is None:
        return
    for current in iterator:
        yield last, current
        last = current


def to_graph(components):
    """Return a networkx graph object from a list of lists of nodes.  Each of
    these lists is interpreted as a set of nodes forming a connected component.

    """
    graph = nx.Graph()
    for component in components:
        graph.add_nodes_from(component)
        graph.add_edges_from(to_edges(component))
    return graph


def get_connected_components(inputs, outputs):
    """Interpreting the clusters as connected components of a graph, return the
    list of merged connected components.  This is done to implement
    transitivity in the clusters.

    """
    transaction_input_addresses = get_transaction_input_addresses(inputs)
    transaction_output_addresses = get_transaction_output_addresses(outputs)
    clusters = [
        transaction_input_addresses[tx] for tx in transaction_input_addresses
    ]

    for tx_id in transaction_output_addresses:
        if tx_id not in transaction_input_addresses:
            clusters.append(transaction_output_addresses[tx_id])
        else:
            tx_input_address_list = transaction_input_addresses[tx_id]
            tx_output_address_list = transaction_output_addresses[tx_id]
            if len(tx_input_address_list) == len(tx_output_address_list) == 1:
                input_address = tx_input_address_list[0]
                output_address = tx_output_address_list[0]
                clusters.append([input_address, output_address])
            else:
                clusters.append(transaction_output_addresses[tx_id])

    return connected_components(to_graph(clusters))


def generate_entity_dict(inputs, outputs):
    """Return a dictionary which maps entity identifiers (integers starting
    from 1) to the corresponding set of addresses (public keys), obtained by
    computing the connected components of the graph represented by the
    clusters.

    """
    components = get_connected_components(inputs, outputs)

    entity_dict = {}
    entity_id = 1
    for component in components:
        entity_dict[entity_id] = component
        entity_id += 1
    return entity_dict


def get_address_to_entity_map(inputs, outputs):
    """Return a dictionary which maps addresses (public keys) to the
    corresponding entity identifier.

    """
    entity_to_address_dict = generate_entity_dict(inputs, outputs)
    address_to_entity_dict = {}

    for entity in entity_to_address_dict:
        addresses = entity_to_address_dict[entity]
        for address in addresses:
            address_to_entity_dict[address] = entity

    return address_to_entity_dict


def get_entity_utxo_ids(inputs, outputs):
    """Return a dictionary which maps each entity ID to the corresponding list
    of UTXO IDs.

    """
    utxo_ids = get_utxo_ids(inputs, outputs)
    address_to_entity_dict = get_address_to_entity_map(inputs, outputs)
    entity_utxo_dict = {}

    for utxo_id in utxo_ids:
        address = outputs[utxo_id]['pk_id']
        entity = address_to_entity_dict[address]
        if entity not in entity_utxo_dict:
            entity_utxo_dict[entity] = []
        entity_utxo_dict[entity].append(utxo_id)
    return entity_utxo_dict


def compute_total_utxo_per_entity(inputs, outputs):
    """Return a dictionary which maps each entity to the corresponding total
    value of its UTXOs.

    """
    entity_utxo_ids_dict = get_entity_utxo_ids(inputs, outputs)
    entity_total_utxo_dict = {}

    for entity in entity_utxo_ids_dict:
        total = 0
        for utxo_id in entity_utxo_ids_dict[entity]:
            total += outputs[utxo_id]['value']
        entity_total_utxo_dict[entity] = total
    return entity_total_utxo_dict


def find_entity_with_highest_total_utxo(inputs, outputs):
    """Find the entity with the highest total amount of UTXO.  Return a
    dictionary containing additional relevant information, such as the entity
    identifier, the maximum total value and the minimum address ( public key)
    in numerical terms belonging to this entity.

    """
    entity_total_utxo_dict = compute_total_utxo_per_entity(inputs, outputs)
    max_total_utxo = 0
    entity_id_with_max_utxo = 0

    for entity_id in entity_total_utxo_dict:
        current_total_utxo = entity_total_utxo_dict[entity_id]
        if current_total_utxo > max_total_utxo:
            max_total_utxo = current_total_utxo
            entity_id_with_max_utxo = entity_id

    # Redundant computation, but whatever...
    entity_to_addresses_dict = generate_entity_dict(inputs, outputs)
    min_entity_address = min(entity_to_addresses_dict[entity_id_with_max_utxo])
    return {
        'id': entity_id_with_max_utxo,
        'value': max_total_utxo,
        'min_address': min_entity_address
    }


def total_value_sent_to_entity_in_tx(transaction, entity_id,
                                     entity_to_address_dict):
    """Return the total value sent by transaction, which is a dictionary
    containing all of its inputs and outputs, to the entity with identifier
    entity_id.

    An additional data structure is used, entity_to_address_dict is a
    dictionary which maps entitiy identifiers to the corresponding set of
    addresses (public keys).

    """
    addresses_in_entity = entity_to_address_dict[entity_id]
    outputs = transaction['outputs']
    total_value = 0

    for output_id in outputs:
        if outputs[output_id]['pk_id'] in addresses_in_entity:
            total_value += outputs[output_id]['value']
    return total_value


def find_highest_tx_sending_to_entity(inputs, outputs, transactions,
                                      entity_id):
    """Return the transaction object (identifier and block identifier) which is
    sending the largest sum to the specified entity_id.

    """
    tx_map = build_transaction_to_inputs_and_outputs_map(inputs, outputs)
    entity_to_address_dict = generate_entity_dict(inputs, outputs)
    max_value = 0
    tx_id_with_max_value = 0

    for tx_id in tx_map:
        current_value = total_value_sent_to_entity_in_tx(
            tx_map[tx_id], entity_id, entity_to_address_dict)
        if current_value > max_value:
            max_value = current_value
            tx_id_with_max_value = tx_id
    return transactions[tx_id_with_max_value]


def find_greatest_transaction_to_highest_utxo_entity(inputs, outputs,
                                                     transactions):
    """Return the transaction sending the highest total amount to the entity
    holding the largest overall amount of UTXO.

    """
    highest_utxo_entity = find_entity_with_highest_total_utxo(inputs, outputs)
    entity_id = highest_utxo_entity['id']
    return find_highest_tx_sending_to_entity(inputs, outputs, transactions,
                                             entity_id)


def build_transaction_to_inputs_and_outputs_map(inputs, outputs):
    """Return a more sophisticated transaction map.  This dictionary maps
    transaction identifiers to a dictionary composed of two fields: 'inputs'
    and 'outputs', which are themselves mapped to the usual input/output
    dictionaries mapped to the respective ids.

    This structure is very convenient to extract information about transaction
    inputs and outputs all in one place.

    """
    tx_map = {}

    for output_id in outputs:
        output = outputs[output_id]
        tx_id = output['tx_id']
        if tx_id not in tx_map:
            tx_map[tx_id] = {'inputs': {}, 'outputs': {}}
        tx_map[tx_id]['outputs'][output_id] = output

    for input_id in inputs:
        current_input = inputs[input_id]
        tx_id = current_input['tx_id']
        if tx_id not in tx_map:
            tx_map[tx_id] = {'inputs': {}, 'outputs': {}}
        tx_map[tx_id]['inputs'][input_id] = current_input

    return tx_map


def convert_inputs_to_use_entities(inputs, address_to_entity_dict):
    """Return a new input dictionary that uses an entity field instead of the
    address (signature).

    """
    entity_inputs = {}
    for input_id in inputs:
        current_input = inputs[input_id]
        address = current_input['sig_id']
        entity = address_to_entity_dict[address]
        entity_inputs[input_id] = {
            'id': current_input['id'],
            'tx_id': current_input['tx_id'],
            'entity': entity,
            'output_id': current_input['output_id']
        }
    return entity_inputs


# Horrible duplication!
def convert_outputs_to_use_entities(outputs, address_to_entity_dict):
    """Return a new output dictionary that uses an entity field instead of the
    address (public key).

    """
    entity_outputs = {}
    for output_id in outputs:
        current_output = outputs[output_id]
        address = current_output['pk_id']
        entity = address_to_entity_dict[address]
        entity_outputs[output_id] = {
            'id': current_output['id'],
            'tx_id': current_output['tx_id'],
            'entity': entity,
            'value': current_output['value']
        }
    return entity_outputs


def get_entity_graph_edges_for_tx(entity_inputs, entity_outputs, tx_map):
    """Return a set of tuples representing the edges of the entity transaction
    graph.  Edges are represented as (in, out) tuple pairs, where 'in' and
    'out' are both themselves tuples of the form ('input'|'output', id).  This
    allows us to differentiate between input and output identifiers in the
    graph.

    """
    entity_edge_pairs = set()

    for input_id in entity_inputs:
        current_input = entity_inputs[input_id]
        current_output = entity_outputs[current_input['output_id']]
        if current_input['entity'] != current_output['entity']:
            entity_edge_pairs.add((('output', current_output['id']),
                                   ('input', current_input['id'])))

    for tx_id in tx_map:
        tx_inputs = tx_map[tx_id]['inputs']
        if tx_inputs:
            for input_id in tx_inputs:
                current_input = tx_inputs[input_id]
                tx_outputs = tx_map[tx_id]['outputs']
                for output_id in tx_outputs:
                    current_output = tx_outputs[output_id]
                    if current_input['entity'] != current_output['entity']:
                        entity_edge_pairs.add(
                            (('input', current_input['id']),
                             ('output', current_output['id'])))
    return entity_edge_pairs


def build_entity_transaction_graph(inputs, outputs):
    """Return the graph object representing transactions between entities
    composed of multiple addresses (public keys).

    """
    address_to_entity_dict = get_address_to_entity_map(inputs, outputs)
    entity_inputs = convert_inputs_to_use_entities(inputs,
                                                   address_to_entity_dict)
    entity_outputs = convert_outputs_to_use_entities(outputs,
                                                     address_to_entity_dict)

    tx_map = build_transaction_to_inputs_and_outputs_map(
        entity_inputs, entity_outputs)
    node_pairs = ([('input', id)
                   for id in entity_inputs] + [('output', id)
                                               for id in entity_outputs])

    graph = nx.DiGraph()
    graph.add_nodes_from(node_pairs)
    edges = get_entity_graph_edges_for_tx(entity_inputs, entity_outputs,
                                          tx_map)
    graph.add_edges_from(list(edges))
    return graph


def find_longest_path_in_entity_graph(inputs, outputs):
    """Return a list representing the longest path in the entity graph.

    """
    graph = build_entity_transaction_graph(inputs, outputs)
    return nx.dag_longest_path(graph)


def main():
    """Main function.  This is going to be executed immediately if the script
    is run directly from the operating system, but it can also be invoked
    manually from an interactive interpreter.

    """
    inputs = filter_malformed_inputs(get_inputs(INPUTS_FILENAME))
    outputs = get_outputs(OUTPUTS_FILENAME)
    transactions = get_transactions(TRANSACTIONS_FILENAME)

    remove_incorrect_inputs(inputs, outputs)

    print('Cumulative UTXO is: ', get_cumulative_utxo(inputs, outputs))
    print()
    print('The highest UTXO is found at ',
          find_highest_utxo(inputs, outputs, transactions))
    print()
    print('The entity with the highest UTXO is ',
          find_entity_with_highest_total_utxo(inputs, outputs))
    print()
    print(
        'The transaction sending the most amount to the entity with the\n'
        'highest total UTXO is ',
        find_greatest_transaction_to_highest_utxo_entity(
            inputs, outputs, transactions))
    print()
    longest_path = find_longest_path_in_entity_graph(inputs, outputs)
    print('The longest path in the entity-to-entity payment graph is ',
          longest_path, ' and its length is ', len(longest_path))


if __name__ == '__main__':
    main()
